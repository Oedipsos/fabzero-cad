// Description: Simple SeaBin model to use in locks.
// Authors: Hallemans Alexandre, Louis Devroye
// Copyright: 2023
// License: MIT License (https://mit-license.org/)

module extrude_2dpath_along_circle(polyline, r, twist=360)
    /* Extrudes given polygonal shape along a circle of radius r, for a given
    angular section.
    :param: polyline Array of 2D points describing a closed shape.
    :param: r Radius of the extrusion circle.
    :param: twist Angular section to be covered.
    
    :example:
    // Extrudes a square of side 4 along a circle of radius 11
    sq = [[0, 0], [0, 4], [4, 4], [4, 0]];
    extrude_2dpath_along_circle(sq, 10);
    */
{
    shape = [ for (p = polyline) p + [r, 0]];
    rotate_extrude(angle=twist, convexity=10)
        polygon(shape);
}

module flexible_output(length, radius, maxSegmentLength = 7, center=false)
{
    n = ceil(length / maxSegmentLength);  // Retrieves number of segments
    l = length / n;
    translate([0, 0, center ? -length/2 : 0])
        union() {
           for (i=[0 : n-1])
              // r1 smaller to insert into tube, r2 larger to tighten. Empiric values.
              translate([0, 0, i*l])
                  cylinder(l, (radius - 0.1), (radius + 0.5));
        };
}

// Units
mm = 1;
cm = 10 * mm;

$fn = 200;
tol = 0.05 * mm;

wallThickness = 5 * mm;
gap = 2.5 * mm;       // Gap between inner and outer box

// Inner box dimensions
H1 = 13.3 * cm;
outerR1 = 10 * cm / 2.;
innerR1 = outerR1 - wallThickness;

// Outer box dimensions
H2 = 14 * cm;
innerR2 = outerR1 + gap;
outerR2 = innerR2 + wallThickness;

// Floating wedge parameters
h = 66.7 * mm;   // Pocket top height
r = 13.3 * mm;   // Max distance from wall
lowP = -33.3 * mm;  // Lowest point
highP = 20 * mm;   // Highest point

// Hopper dimensions
hopperHeight = 3 * cm;
connectionLength = 2.5 * cm;
pipeSize = 20 * mm / 2.;
outputR = pipeSize - 1.6 * mm; 
flexible = true;

// Floating wedge shape
shape1 = [[0, 0], [-r, lowP], [0, highP]]; shape = shape1;
//shape2 = [[0, 0], [-r, lowP], [-r, H-h], [0, H-h]]; shape = shape2;

// Outer box
difference() {
    union() {
        // Main body
        cylinder(h=H2, r=outerR2);
        // Hopper
        translate([0, 0, -hopperHeight])
            cylinder(hopperHeight, pipeSize, outerR2);
        // Pipe output
        if (flexible)
            translate([0, 0, -hopperHeight - connectionLength])
                flexible_output(connectionLength, pipeSize);
        else
            translate([0, 0, -hopperHeight - connectionLength])
                cylinder(h=connectionLength, r=pipeSize);
    };
    // Main body
    translate([0, 0, -tol])
        cylinder(H2+2*tol, r=innerR2);
    // Hopper
    translate([0, 0, -hopperHeight])
        cylinder(hopperHeight, outputR, innerR2);
    // Pipe output
    translate([0, 0, -hopperHeight - connectionLength - tol])
        cylinder(h=connectionLength+2*tol, r=outputR);
    // Fixation holes
    for (t = [0 : 90 : 359])
        rotate([0, 0, t])
        translate([innerR2 - wallThickness/2, 0, H2 - 16*mm])
        rotate([0, 90, 0])
            cylinder(h=2*wallThickness, r=4 * mm);
};

// Inner box
translate([0, 0, (H2 - H1) - tol])
    union() {
        difference() {
            cylinder(h=H1, r=outerR1);
            translate([0, 0, -tol])
                cylinder(h=H1+2*tol, r=innerR1);
        };
        // Floating wedge
        translate([0, 0, h])
            extrude_2dpath_along_circle(shape, r=innerR1+tol);
    };
    

