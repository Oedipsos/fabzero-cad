// Description: Spring-like LEGO compatible part.
// Author: Hallemans Alexandre
// Copyright: 2023
// License: MIT License (https://mit-license.org/) 

include <BOSL/constants.scad>
use <BOSL/paths.scad>

$fn = 100;

module brick(hholes, vholes, r=2.5, s=8, h=10, e=1.6, center=false, pins=true, holes=true) {
    l = hholes * s;
    w = vholes * s; 
    translate(center ? [-l/2, -w/2, -h/2] : [0, 0, 0])
    union() {
        difference() {
            cube([l, w, h]);
            
            if(holes) {
                for(i = [0:hholes-1], j = [0:vholes-1]) {  // Holes
                    translate([s * (i+0.5), s * (j+0.5), -0.01])
                        cylinder(e+0.01, r=r+0.01);   
                };
            };
        };
        if(pins) {
            for(i = [0:hholes-1], j = [0:vholes-1]) {  // Pins
                translate([s * (i+0.5), s * (j+0.5), h-0.01])
                    cylinder(e+0.01, r=r);
            };   
        };    
    };
};

// ==== Parameters ====

nturns = 1.75;

// Block dimensions

lb = 24;  // x-size
wb = 8;   // y-size
hb = 5;   // z-size

// Spiral thickness
sth = 3;

// Spiral track width
stw = 1.8;

// Spiral total height
sh = 30;

// Spiral radius
sr = lb / 2 - stw - 0.01;

// Track shape
shape = [[0, 0], [0, sth], [stw, sth], [stw, 0]];

// ==== Building piece ====

// Base block, pins are not needed
translate([0, 0, hb/2])
    brick(3, 1, s=wb, h=hb, center=true);

// Upper block
translate([0, 0, hb/2 + sh])
rotate([0, 0, nturns*360])
    brick(3, 1, s=wb, h=hb, center=true);

// Double spiral
extrude_2dpath_along_spiral(shape, h=sh, r=sr, twist=nturns*360, $fn=100); 
rotate([0, 0, 180])
    extrude_2dpath_along_spiral(shape, h=sh, r=sr, twist=nturns*360, $fn=100); 

// ==== Supports ====

// Block base bridging platform
mirror([0, 1, 0])
translate([0, sr+stw/2, sh-0.2])
    cube([wb, stw, 0.4], center=true);

translate([0, sr+stw/2, sh-0.2])
    cube([wb, stw, 0.4], center=true);
    
// Support beams
for (t = [0 : 45 : 359])
    rotate([0, 0, th])
    translate([sr + stw/2, 0, 0])
        cylinder(h=sh, r=stw/2-0.01);

