// Description: Custom bricks for real-life measurement tests
// Authors: Hallemans Alexandre
// Copyright: 2023
// License: MIT License (https://mit-license.org/)

$fn = 100;

function lerp(A, B, t) = A*(1-t) + B*t;

module brick(hholes, vholes, r=2.5, s=10, h=10, e=2, center=false) {
    l = hholes * s;
    w = vholes * s; 
    translate(center ? [-l/2, -w/2, -h/2] : [0, 0, 0])
    union() {
        difference() {
            cube([l, w, h]);
            
            for(i = [0:hholes-1], j = [0:vholes-1]) {  // Holes
                translate([s * (i+0.5), s * (j+0.5), -0.01])
                    cylinder(e+0.01, r=r+0.1);   
            };
        };
        for(i = [0:hholes-1], j = [0:vholes-1]) {  // Pins
            translate([s * (i+0.5), s * (j+0.5), h-0.01])
                cylinder(e+0.01, r=r);   
        };    
    };
};

module r_check() {
    // Height of the main block
    height = 10;

    // Width of the main block
    width = 9; 

    // Number of holes wanted
    nholes = 11;  

    // Minimum and maximum diameters of the hole series
    minD = 4.5; // [0:width/2]
    maxD = 5.5; // [minD:width/2]

    // ---------------------------------------
    
    spacing = 10;
    length = (nholes-1) * spacing;

    difference() {
        union() {     // Create the main block
            cube([length, width, height]);
            translate([0, width/2, 0])
                cylinder(height, d=width);
            translate([length, width/2, 0])
                cylinder(height, d=width);
        };

        for (l = [0 : spacing : length])  // Make equidistant holes with increasing radius
            translate([l, width/2, -0.01])
                cylinder(height+0.02, d=lerp(minD, maxD, l/length));

    translate([0, 0.5, height/2])
    rotate([0, 90, -90])
    linear_extrude(1)
        text(str(minD), 4, halign="center", valign="bottom");
    
    translate([length, 0.5, height/2])
    rotate([0, 90, -90])
    linear_extrude(1)
        text(str(maxD), 4, halign="center", valign="top");
         
    };


};


module s_check()
{
    height = 5;
    width = 9; 

    nb = 11;  // number of bricks

    min_s = 7.5; 
    max_s = 8.5; 

    union() {
        for(i = [0 : nb-1]) {
            s = lerp(min_s, max_s, i/(nb-1));
            translate([(s-i/10) * i, 0, 0])
                brick(1, 2, s=s, h=height, center=true);
        };
    };
};

s_check();
translate([0, 20, 0])
    r_check();
