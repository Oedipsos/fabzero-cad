# OpenSCAD work at ULB

All the files found here are the different objects modeled for the PHYS-F517 course at ULB, "How to make (almost) any
experiment using digital fabrication".

The LockBin files are used in our final project, and therefore I co-authored the file with some of my group members.

You can go read my [student page](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/alexandre.hallemans/)
and our [group project page](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/alexandre.hallemans/) as well.

